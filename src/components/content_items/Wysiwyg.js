// A component when drap and dropped adds a paragraph to a layout

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { DragSource } from 'react-dnd';

import { addItemToLayout,addWysiwygToLayout } from '../../actions';

const spec = {
    beginDrag(props) 
    {
        console.log("BD");
        return {};
    },
    endDrag(props, monitor, component) 
    {
        window.component_monitor = monitor;
        console.log("END DRAG WYSIWYG",this,arguments);
        if(monitor.didDrop())
        {
            console.log("ED content_item WYSIWYG spec",this);
            const data = `WYSIWYG Ullamco dolore commodo ad do ea
            mollit fugiat aliquip sit Lorem. Do fugiat in laborum enim 
            duis veniam reprehenderit. Nulla velit quis excepteur proiden
            t exercitation.`;
            // On drop call a action to add paragraph to layout
            if(true || monitor.getDropResult().layout_id && monitor.getDropResult().location)
            {
                props.addWysiwygToLayout(
                    data,
                    monitor.getDropResult().layout_id,
                    monitor.getDropResult().location,
                    "wysiwyg"
                );
            }
        }
        else
        {
            return;
        }
    }
}

function collect(connect, monitor)
{
    return {
        connectDragSource: connect.dragSource(),
        connectDragPreview: connect.dragPreview(),
        isDragging: monitor.isDragging()
    }
}

class Wysiwyg extends Component {
    render() {
        const { connectDragSource, connectDragPreview, isDragging } = this.props;

        return connectDragSource(
            <div className="box" >
                <img src="img/content-icon/paragraph-icon.png" alt="" className="img-fluid mx-auto d-block" width="90%"/>
                <p className="mb-0">Wysiwyg</p>
            </div>
        );
    }
}

export default connect(null, { 
    addWysiwygToLayout
 })(DragSource('item', spec, collect)(Wysiwyg));
